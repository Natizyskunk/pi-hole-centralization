# pi-hole-centralization
Everything I use with pi-hole in one place (lists, scripts, tools, themes, guides, etc...).

## Lists
- [pi-hole-lists](https://gitlab.com/Natizyskunk/pi-hole-lists) from [@Natizyskunk](https://gitlab.com/Natizyskunk)
- [dbl.oisd.nl | Internet's #1 domain blocklist](https://www.reddit.com/r/oisd_blocklist/comments/dwxgld/dbloisdnl_internets_1_domain_blocklist/) from [@sjhgvr](https://reddit.com/user/sjhgvr)
- [Hosts files collection for ads and tracking](https://www.github.developerdan.com/hosts) from [@lightswitch05](https://github.com/lightswitch05)

## Scripts
- [pi-hole-scripts](https://gitlab.com/Natizyskunk/pi-hole-scripts) from [@Natizyskunk](https://gitlab.com/Natizyskunk)

## Tools
- [pi-hole-helpers](https://gitlab.com/Natizyskunk/pi-hole-helpers) from [@Natizyskunk](https://gitlab.com/Natizyskunk) (forked from [pi-hole-helpers](https://github.com/Kevin-De-Koninck/pi-hole-helpers) from [@Kevin-De-Koninck](https://github.com/Kevin-De-Koninck/))
- [pi-hole-expand-domain-list](https://gitlab.com/Natizyskunk/pi-hole-expand-domain-list) from [@Natizyskunk](https://gitlab.com/Natizyskunk) (forked from [pi-hole-helpers](https://github.com/Kevin-De-Koninck/pi-hole-helpers) from [@Kevin-De-Koninck](https://github.com/Kevin-De-Koninck/))
- [PADD](https://github.com/jpmck/PADD) from [@jpmck](https://github.com/jpmck)
- [pihole-speedtest](https://github.com/arevindh/pihole-speedtest) from [@arevindh](https://github.com/arevindh)
- [pi-hole-dns-over-tls](https://gitlab.com/Natizyskunk/pi-hole-dns-over-tls) from [@Natizyskunk](https://gitlab.com/Natizyskunk)

## Themes
- [PiHole-Dark](https://github.com/lkd70/PiHole-Dark) from [@lkd70](https://github.com/lkd70)
- [pi-hole-material-dark](https://github.com/MBarrows20/pi-hole-material-dark) from [@MBarrows20](https://github.com/MBarrows20)
- [pi-hole-midnight](https://github.com/jacobbates/pi-hole-midnight) from [@jacobbates](https://github.com/jacobbates)
- [pihole-dark-theme](https://userstyles.org/styles/175110/pihole-dark-theme) from [@dan0v](https://userstyles.org/users/847859)

## Guides
- [Editing Whitelist and Blacklist](https://docs.pi-hole.net/guides/whitelist-blacklist) from [Pi-hole Official Documentation](https://docs.pi-hole.net)
- [Pi-hole regular expressions tutorial](https://docs.pi-hole.net/ftldns/regex/tutorial) from [Pi-hole Official Documentation](https://docs.pi-hole.net)
- [Hostname naming convention](https://gitlab.com/Natizyskunk/hostname-naming-convention) from [@Natizyskunk](https://gitlab.com/Natizyskunk)
- [Pi-hole as All-Around DNS Solution (unboud)](https://docs.pi-hole.net/guides/unbound) from [Pi-hole Official Documentation](https://docs.pi-hole.net)
- [Configuring DNS-Over-HTTPS on Pi-hole](https://docs.pi-hole.net/guides/dns-over-https) from [Pi-hole Official Documentation](https://docs.pi-hole.net)
- [Configuring DNS-Over-TLS on Pi-Hole](https://bartonbytes.com/posts/configure-pi-hole-for-dns-over-tls) from [@Barton Bytes](https://bartonbytes.com)
